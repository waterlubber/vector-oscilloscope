import vars
#import synthesizer
import pyaudio
import time
from math import sin
import array
xData = array.array('h', [int(sin(x/5) * 32768) for x in range(0, vars.sample_rate)])
yData = array.array('h', [int(sin(3.141592 + x/5) * 32768) for x in range(0, vars.sample_rate)])
data = array.array('h', [val for pair in zip(xData, yData) for val in pair])
data = data.tobytes()
def synthesize(in_data,n_frames, time_info, status):
    # callback function for pyaudio
    #xData = array.array('h', [int(sin(x) * 32768) for x in range(0, vars.sample_rate)])
    #yData = array.array('h', [int(sin(3.14+x) * 32768) for x in range(0, vars.sample_rate)])

    #print(data)
    return data, pyaudio.paContinue

pa = pyaudio.PyAudio()
stream = pa.open(rate=vars.sample_rate, channels=2, format=vars.audio_format, output=True, stream_callback=synthesize)
stream.start_stream()
while stream.is_active():
    time.sleep(.05)