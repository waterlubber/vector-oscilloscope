import pyaudio
import vars
import array
from math import sin
import playback

def synthesize(in_data,n_frames, time_info, status):
    # callback function for pyaudio
    #xData = array.array('h', [int(sin(x) * 32768) for x in range(0, vars.sample_rate)])
    #yData = array.array('h', [int(sin(3.14+x) * 32768) for x in range(0, vars.sample_rate)])
    data = array.array('h', [val for pair in zip(playback.xData, playback.yData) for val in pair])
    print(data)
    print('\n')
    data = data.tobytes()
    #print(data)
    return data, pyaudio.paContinue
